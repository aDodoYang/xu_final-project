﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Player : MonoBehaviour
{


    public float speed = 5;
    public float turnspeed;

    // Start is called before the first frame update





    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        transform.Translate(x * turnspeed * Time.deltaTime, 0, speed * Time.deltaTime);

        if (transform.position.y < -10)
        {
            Time.timeScale = 0;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
            Time.timeScale = 1;
            return;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle"))
        {
            this.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Accelerator"))
        {
            speed = speed + 5;
                }
    }

}

